# FinalProject
## Tech Stack:
- Java
- [Redis](https://redis.io/)
- [Lettuce](https://lettuce.org)
- [fastjson](https://github.com/alibaba/fastjson)
## How to Build
- install redis on your computer
- $redis-server
- Build&Run
## Why need to install Redis on my own?
Because we can't afford a server.:(
