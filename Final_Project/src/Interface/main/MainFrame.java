/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Interface.main;

import DAO.DB4OUtil;
import Interface.Management.PM;
import Interface.OA.SAOP.OA_SAOP_1;
import Interface.Management.FM;
import Interface.Management.EM;
import Interface.DPC.VMT.DPC_VMT;
import Interface.DPC.JP.DPC_JP;
import Interface.DPC.VAP.DPC_VAP;
import Interface.DPC.JP.DPC_JP_PNJ;
import Interface.Inbox.MailInbox;
import Interface.Management.Analysis;
import Interface.OA.MOAR.OA_MOAR;
import Interface.OA.VAOA.OA_VAOA;
import Model.EcoSystem.Company;
import Model.EcoSystem.EcoSystem;
import Model.Interface.Employee;
import java.util.List;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

/**
 *
 * @author Lins
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    private LogStatus logStatus;
    private EcoSystem system;
    public MainFrame() {
        system = DB4OUtil.getInstance().retrieveSystem();
        initComponents();
        this.setSize(700, 750);
        this.setMenuDisable();
        LoginJPanel login = new LoginJPanel(this);
        this.setContentPane(login);
        login.updateUI();
        this.logStatus = new LogStatus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        Account = new javax.swing.JMenu();
        Home = new javax.swing.JMenuItem();
        Inbox = new javax.swing.JMenuItem();
        Info = new javax.swing.JMenuItem();
        Quit = new javax.swing.JMenuItem();
        OA = new javax.swing.JMenu();
        SAOP = new javax.swing.JMenuItem();
        VAOA = new javax.swing.JMenuItem();
        MOR = new javax.swing.JMenuItem();
        DPC = new javax.swing.JMenu();
        VJ = new javax.swing.JMenuItem();
        VAP = new javax.swing.JMenuItem();
        VMT = new javax.swing.JMenuItem();
        Management = new javax.swing.JMenu();
        FM = new javax.swing.JMenuItem();
        PM = new javax.swing.JMenuItem();
        EM = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jMenuBar1.setForeground(new java.awt.Color(0, 102, 204));

        Account.setText("Account");

        Home.setText("Home");
        Home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HomeActionPerformed(evt);
            }
        });
        Account.add(Home);

        Inbox.setText("Inbox");
        Inbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InboxActionPerformed(evt);
            }
        });
        Account.add(Inbox);

        Info.setText("Info");
        Info.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InfoActionPerformed(evt);
            }
        });
        Account.add(Info);

        Quit.setText("Quit");
        Quit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuitActionPerformed(evt);
            }
        });
        Account.add(Quit);

        jMenuBar1.add(Account);

        OA.setText("OA System");

        SAOP.setText("Start An OA Process");
        SAOP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SAOPActionPerformed(evt);
            }
        });
        OA.add(SAOP);

        VAOA.setText("View All My OA Sessions");
        VAOA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VAOAActionPerformed(evt);
            }
        });
        OA.add(VAOA);

        MOR.setText("My OA Review");
        MOR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MORActionPerformed(evt);
            }
        });
        OA.add(MOR);

        jMenuBar1.add(OA);

        DPC.setText("DPC System");

        VJ.setText("Job Pool");
        VJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VJActionPerformed(evt);
            }
        });
        DPC.add(VJ);

        VAP.setText("View All Projects");
        VAP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VAPActionPerformed(evt);
            }
        });
        DPC.add(VAP);

        VMT.setText("View My Tickets");
        VMT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VMTActionPerformed(evt);
            }
        });
        DPC.add(VMT);

        jMenuBar1.add(DPC);

        Management.setText("Management");

        FM.setText("Finacial Management");
        FM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FMActionPerformed(evt);
            }
        });
        Management.add(FM);

        PM.setText("Property Management");
        PM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PMActionPerformed(evt);
            }
        });
        Management.add(PM);

        EM.setText("Employee Management");
        EM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EMActionPerformed(evt);
            }
        });
        Management.add(EM);

        jMenuItem1.setText("Company Analysis");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        Management.add(jMenuItem1);

        jMenuBar1.add(Management);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 278, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public LogStatus getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(LogStatus logStatus) {
        this.logStatus = logStatus;
    }
    
    public JMenuBar getjMenuBar(){
        return this.jMenuBar1;
    }
    
    public void setMenuEnable(){
        this.jMenuBar1.setVisible(true);
    }
    
    public void setMenuDisable(){
        this.jMenuBar1.setVisible(false);
    }
    
    public EcoSystem getSystem(){
        return this.system;
    }

    private void InboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InboxActionPerformed
        // TODO add your handling code here:
        MailInbox mi = new MailInbox(this);
        this.setContentPane(mi);
        mi.updateUI();

    }//GEN-LAST:event_InboxActionPerformed

    private void VAPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VAPActionPerformed
        // TODO add your handling code here:
        DPC_VAP viewAllProject = new DPC_VAP(this);
        this.setContentPane(viewAllProject);
        viewAllProject.updateUI();
    }//GEN-LAST:event_VAPActionPerformed

    private void SAOPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SAOPActionPerformed
        // TODO add your handling code here:
        OA_SAOP_1 startAnOATicket = new OA_SAOP_1(this);
        this.setContentPane(startAnOATicket);
        startAnOATicket.updateUI();
    }//GEN-LAST:event_SAOPActionPerformed

    private void VMTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VMTActionPerformed
        // TODO add your handling code here:
        DPC_VMT viewMyTicket = new DPC_VMT(this);
        this.setContentPane(viewMyTicket);
        viewMyTicket.updateUI();
    }//GEN-LAST:event_VMTActionPerformed

    private void VAOAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VAOAActionPerformed
        // TODO add your handling code here:
        OA_VAOA vaoa = new OA_VAOA(this);
        this.setContentPane(vaoa);
        vaoa.updateUI();
    }//GEN-LAST:event_VAOAActionPerformed

    private void MORActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MORActionPerformed
        // TODO add your handling code here:
       OA_MOAR moar = new OA_MOAR(this);
       this.setContentPane(moar);
       moar.updateUI();

    }//GEN-LAST:event_MORActionPerformed

    private void InfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InfoActionPerformed
        // TODO add your handling code here:
        InfoJPanel info = new InfoJPanel(this);
        this.setContentPane(info);
        info.updateUI();
    }//GEN-LAST:event_InfoActionPerformed

    private void QuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QuitActionPerformed
        // TODO add your handling code here:
        int input = JOptionPane.showConfirmDialog(null, "Do you want to Sign out?", "Quit",
                JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

        // 0=yes, 1=no
        if (input == 0) {
            modMenu(this.logStatus.getUser());
            DB4OUtil.getInstance().storeSystem(system);
            LoginJPanel login = new LoginJPanel(this);
            this.setMenuDisable();
            this.setContentPane(login);
            login.updateUI();
            this.logStatus = new LogStatus();
        }
    }//GEN-LAST:event_QuitActionPerformed

    private void modMenu(Employee e){
        JMenuBar jmb = this.getJMenuBar();
        boolean flag = true;
        switch(e.getDepartment()){
            case "Admin":
                jmb.getMenu(2).setVisible(flag);
                jmb.getMenu(3).getItem(3).setVisible(flag);
                jmb.getMenu(3).getItem(0).setVisible(flag);
                jmb.getMenu(3).getItem(2).setVisible(flag);
                break;
            case "Accountant":
                jmb.getMenu(2).setVisible(flag);
                jmb.getMenu(3).getItem(3).setVisible(flag);
                jmb.getMenu(3).getItem(1).setVisible(flag);
                jmb.getMenu(3).getItem(2).setVisible(flag);
                break;
            case "Developer":
                jmb.getMenu(3).setVisible(flag);
                jmb.getMenu(2).getItem(0).setVisible(flag);
                break;
            case "PM":
                jmb.getMenu(3).setVisible(flag);
                break;
            case "Recruiter":
                jmb.getMenu(2).setVisible(flag);
                jmb.getMenu(3).getItem(3).setVisible(flag);
                jmb.getMenu(3).getItem(0).setVisible(flag);
                jmb.getMenu(3).getItem(1).setVisible(flag);
                break;
            default:
                break;
        }
    }
    
    private void VJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VJActionPerformed
        // TODO add your handling code here:
        DPC_JP viewDemands = new DPC_JP(this);
        this.setContentPane(viewDemands);
        viewDemands.updateUI();
    }//GEN-LAST:event_VJActionPerformed

    private void FMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FMActionPerformed
        // TODO add your handling code here:
        FM finacial = new FM(this);
        this.setContentPane(finacial);
        finacial.updateUI();
    }//GEN-LAST:event_FMActionPerformed

    private void PMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PMActionPerformed
        // TODO add your handling code here:
        PM property = new PM(this);
        this.setContentPane(property);
        property.updateUI();
    }//GEN-LAST:event_PMActionPerformed

    private void EMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EMActionPerformed
        // TODO add your handling code here:
        EM es = new EM(this);
        this.setContentPane(es);
        es.updateUI();

    }//GEN-LAST:event_EMActionPerformed

    private void HomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HomeActionPerformed
        // TODO add your handling code here:
        WelcomeJpanel wel = new WelcomeJpanel(this);
        this.setContentPane(wel);
        wel.updateUI();
    }//GEN-LAST:event_HomeActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        Analysis ap = new Analysis(this);
        this.setContentPane(ap);
        ap.updateUI();
    }//GEN-LAST:event_jMenuItem1ActionPerformed


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);

            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Account;
    private javax.swing.JMenu DPC;
    private javax.swing.JMenuItem EM;
    private javax.swing.JMenuItem FM;
    private javax.swing.JMenuItem Home;
    private javax.swing.JMenuItem Inbox;
    private javax.swing.JMenuItem Info;
    private javax.swing.JMenuItem MOR;
    private javax.swing.JMenu Management;
    private javax.swing.JMenu OA;
    private javax.swing.JMenuItem PM;
    private javax.swing.JMenuItem Quit;
    private javax.swing.JMenuItem SAOP;
    private javax.swing.JMenuItem VAOA;
    private javax.swing.JMenuItem VAP;
    private javax.swing.JMenuItem VJ;
    private javax.swing.JMenuItem VMT;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    // End of variables declaration//GEN-END:variables
}
