/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Interface.main;

import Model.EcoSystem.Company;
import Model.EcoSystem.Employer;
import Model.Interface.Employee;
import Model.Users.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lins
 */
public class LogStatus {
    private Company company;
    private Employee user;
    private Employer employer;
    private String status;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    public Employee getUser() {
        return user;
    }

    public void setUser(Employee user) {
        this.user = user;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
