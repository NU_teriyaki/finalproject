/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.DPC.JP;

import Interface.main.MainFrame;
import Model.EcoSystem.EcoSystem;
import Model.EcoSystem.Job;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author guhandan
 */
public class DPC_JP extends javax.swing.JPanel {

    /**
     * Creates new form DPC_ViewAllPro_CreatePro
     */
    private MainFrame mainFrame;
    private EcoSystem system;
    public DPC_JP(MainFrame mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        system = mainFrame.getSystem();
        refreshTable();
    }

    public void refreshTable() {
        int rowCount = this.jTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (Job job:this.system.getJobs()) {
            Object row[] = new Object[4];
            row[0] = job;
            row[1] = job.getPostDate();
            row[2] = job.getPoster();
            row[3] = job.getBudget();
            model.addRow(row);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        ViewBtn = new javax.swing.JButton();
        PostBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setForeground(new java.awt.Color(0, 102, 204));

        jTable.setForeground(new java.awt.Color(0, 102, 204));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Job Title", "Create Date", "Poster", "Profit"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 545, 110));

        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/navigation.png"))); // NOI18N
        jLabel1.setText("DPC System >> Job Pool");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        ViewBtn.setBackground(new java.awt.Color(255, 255, 255));
        ViewBtn.setForeground(new java.awt.Color(0, 102, 204));
        ViewBtn.setText("VIEW");
        ViewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewBtnActionPerformed(evt);
            }
        });
        add(ViewBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 230, -1, -1));

        PostBtn.setBackground(new java.awt.Color(255, 255, 255));
        PostBtn.setForeground(new java.awt.Color(0, 102, 204));
        PostBtn.setText("POST NEW JOB");
        PostBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PostBtnActionPerformed(evt);
            }
        });
        add(PostBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 230, -1, -1));

        jLabel2.setForeground(new java.awt.Color(0, 102, 204));
        jLabel2.setText("---------------------------------------------------------------------------------");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jLabel5.setForeground(new java.awt.Color(0, 102, 204));
        jLabel5.setText("--------------------------TERIYAKI Version 2.0 -------------------------");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 660, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void ViewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewBtnActionPerformed
        // TODO add your handling code here:
        if(jTable.getSelectedRow() < 0){
            JOptionPane.showMessageDialog(null, "Please Select!", "WARNING", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Job j = (Job) jTable.getValueAt(jTable.getSelectedRow(), 0);
        System.err.println("this is in job pool:"+j.getPostDate());
        DPC_JP_View v = new DPC_JP_View(mainFrame, j);
        mainFrame.setContentPane(v);
        v.updateUI();
    }//GEN-LAST:event_ViewBtnActionPerformed

    private void PostBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PostBtnActionPerformed
        // TODO add your handling code here:
        DPC_JP_PNJ pnj = new DPC_JP_PNJ(mainFrame);
        mainFrame.setContentPane(pnj);
        pnj.updateUI();
        
    }//GEN-LAST:event_PostBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton PostBtn;
    private javax.swing.JButton ViewBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
