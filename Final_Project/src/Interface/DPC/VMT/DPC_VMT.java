/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.DPC.VMT;

import Interface.main.MainFrame;
import Model.Interface.Employee;
import Model.Projects.Project;
import Model.Projects.ProjectPool;
import Model.TicketSystem.OATicket;
import Model.TicketSystem.Ticket;
import Model.TicketSystem.WorkTicket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author guhandan
 */
public class DPC_VMT extends javax.swing.JPanel {

    /**
     * Creates new form DPC_VMT_Assigner
     */
    private MainFrame mainFrame;
    private List<WorkTicket> myTickets;

    public DPC_VMT(MainFrame mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        this.myTickets = new ArrayList<>();
        getWorkTicketByMe();
        refreshTable();
    }

    private void getWorkTicketByMe() {
        Employee current = mainFrame.getLogStatus().getUser();
        for (String p : mainFrame.getLogStatus().getCompany().getProjects()) {
            Project pp = mainFrame.getLogStatus().getCompany().getProjectPool().getProject(p);
            for (WorkTicket wt : pp.getTickets()) {
                if (wt.getReporterID().equals(current.getUID())||wt.getAssignerID().equals(current.getUID())) {
                    myTickets.add(wt);
                }
            }
        }
        Collections.sort(myTickets, new Comparator<WorkTicket>() {
            @Override
            public int compare(WorkTicket o1, WorkTicket o2) {
                if (o1.getPriority()> o2.getPriority()) {
                    return 1;
                } else if (o1.getPriority()< o2.getPriority()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }

    public void refreshTable() {
        int rowCount = this.jTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (Ticket t : this.myTickets) {
            Object row[] = new Object[7];
            row[0] = t;
            row[1] = ((WorkTicket)t).getWorkType();
            row[2] = t.getCreateDate();
            row[3] = t.getReporterID();
            row[4] = t.getAssignerID();
            row[5] = ((WorkTicket)t).getPriority();
            row[6] = ((WorkTicket)t).getWorkStatus();
            model.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        ViewBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setForeground(new java.awt.Color(0, 102, 204));

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ticket ID", "Ticket Type", "Create Date", "Reporter", "Assigner", "Priority", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 588, 230));

        ViewBtn.setBackground(new java.awt.Color(255, 255, 255));
        ViewBtn.setForeground(new java.awt.Color(0, 102, 204));
        ViewBtn.setText("VIEW");
        ViewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewBtnActionPerformed(evt);
            }
        });
        add(ViewBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 360, -1, -1));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/navigation.png"))); // NOI18N
        jLabel1.setText("DPC System >> View My Tickets");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setForeground(new java.awt.Color(0, 102, 204));
        jButton1.setText("UPDATE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 360, -1, 30));

        jLabel2.setForeground(new java.awt.Color(0, 102, 204));
        jLabel2.setText("---------------------------------------------------------------------------------");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jLabel5.setForeground(new java.awt.Color(0, 102, 204));
        jLabel5.setText("--------------------------TERIYAKI Version 2.0 -------------------------");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 660, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void ViewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewBtnActionPerformed
        // TODO add your handling code here:
        if (jTable.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(null, "Please Select!", "WARNING", JOptionPane.WARNING_MESSAGE);
            return;
        }
        WorkTicket wt = (WorkTicket) jTable.getValueAt(jTable.getSelectedRow(), 0);
        String p = wt.getRelatedProj();
        Project project = mainFrame.getLogStatus().getCompany().getProjectPool().getProject(p);
        DPC_VMT_View v = new DPC_VMT_View(mainFrame, wt,project);
        mainFrame.setContentPane(v);
        v.updateUI();
    }//GEN-LAST:event_ViewBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (jTable.getSelectedRow() < 0) {
            JOptionPane.showMessageDialog(null, "Please Select!", "WARNING", JOptionPane.WARNING_MESSAGE);
            return;
        }
        WorkTicket wt = (WorkTicket) jTable.getValueAt(jTable.getSelectedRow(), 0);
        MainFrame mf = new MainFrame();
        DPC_VMT_Update dvu = new DPC_VMT_Update(wt, mf, mainFrame, this);
        mf.setContentPane(dvu);
        mf.setSize(200, 200);
        mf.setVisible(true);
        dvu.updateUI();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ViewBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
