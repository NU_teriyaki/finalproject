/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.DPC.VMT;

import Interface.DPC.VMT.DPC_VMT;
import Interface.main.MainFrame;
import Model.Projects.Project;
import Model.TicketSystem.WorkTicket;

/**
 *
 * @author guhandan
 */
public class DPC_VMT_View extends javax.swing.JPanel {

    /**
     * Creates new form DPC_VMT_RA_View
     */
    private MainFrame mainFrame;
    private WorkTicket wt;
    private Project project;
    public DPC_VMT_View(MainFrame mainFrame,WorkTicket wt,Project project) {
        initComponents();
        this.mainFrame = mainFrame;
        this.project = project;
        this.wt = wt;
        
        ProjectNameFld.setEditable(false);
        PCreateDateFld.setEditable(false);
        ManagerFld.setEditable(false);
        PosterFld.setEditable(false);
        PStatusFld.setEditable(false);
        PDescriptionTArea.setEditable(false);
        
        TicketTitleFld.setEditable(false);
        TCreateDateFld.setEditable(false);
        PriorityFld.setEditable(false);
        TypeFld.setEditable(false);
        ReporterFld.setEditable(false);
        AssignerFld.setEditable(false);
        TStatusFld.setEditable(false);
        TDescriptionTArea.setEditable(false);
        
        ProjectNameFld.setText(project.getName());
        PCreateDateFld.setText(project.getCreateDate().toString());
        ManagerFld.setText(project.getLeaderUID());
        PosterFld.setText(project.getClient());
        PStatusFld.setText(project.getStatus());
        PDescriptionTArea.setText(project.getDescription());          
        
        TicketTitleFld.setText(wt.getTitle());
        TCreateDateFld.setText(wt.getCreateDate().toString());
        PriorityFld.setText(String.valueOf(wt.getPriority()));
        TypeFld.setText(wt.getWorkType());
        ReporterFld.setText(wt.getReporterID());
        AssignerFld.setText(wt.getAssignerID());
        TStatusFld.setText(wt.getWorkStatus().toString());
        TDescriptionTArea.setText(wt.getContent());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BackBtn = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        ProjectNameFld = new javax.swing.JTextField();
        PCreateDateFld = new javax.swing.JTextField();
        ManagerFld = new javax.swing.JTextField();
        PosterFld = new javax.swing.JTextField();
        PStatusFld = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        PDescriptionTArea = new javax.swing.JTextArea();
        TicketTitleFld = new javax.swing.JTextField();
        TCreateDateFld = new javax.swing.JTextField();
        PriorityFld = new javax.swing.JTextField();
        TypeFld = new javax.swing.JTextField();
        ReporterFld = new javax.swing.JTextField();
        AssignerFld = new javax.swing.JTextField();
        TStatusFld = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        TDescriptionTArea = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BackBtn.setBackground(new java.awt.Color(255, 255, 255));
        BackBtn.setForeground(new java.awt.Color(0, 102, 204));
        BackBtn.setText("BACK");
        BackBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackBtnActionPerformed(evt);
            }
        });
        add(BackBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 550, -1, -1));

        jLabel9.setForeground(new java.awt.Color(0, 102, 204));
        jLabel9.setText("Create Date");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 100, -1, -1));

        jLabel10.setForeground(new java.awt.Color(0, 102, 204));
        jLabel10.setText("Poster");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 130, -1, -1));

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 204));
        jLabel6.setText("INFORMATION");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        jLabel11.setForeground(new java.awt.Color(0, 102, 204));
        jLabel11.setText("Status");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jLabel7.setForeground(new java.awt.Color(0, 102, 204));
        jLabel7.setText("Project Name");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, -1, -1));

        jLabel12.setForeground(new java.awt.Color(0, 102, 204));
        jLabel12.setText("Description");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, -1, -1));

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 102, 204));
        jLabel13.setText("TICKETS");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, -1, -1));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/navigation.png"))); // NOI18N
        jLabel1.setText("DPC System >> View My Tickets >> View");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jLabel8.setForeground(new java.awt.Color(0, 102, 204));
        jLabel8.setText("Manager");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, -1, -1));

        jLabel14.setForeground(new java.awt.Color(0, 102, 204));
        jLabel14.setText("Type");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 350, -1, -1));

        jLabel15.setForeground(new java.awt.Color(0, 102, 204));
        jLabel15.setText("Reporter");
        add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, -1, -1));

        jLabel21.setForeground(new java.awt.Color(0, 102, 204));
        jLabel21.setText("Status");
        add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, -1, -1));

        jLabel16.setForeground(new java.awt.Color(0, 102, 204));
        jLabel16.setText("Description");
        add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 450, -1, -1));

        jLabel20.setForeground(new java.awt.Color(0, 102, 204));
        jLabel20.setText("Assigner");
        add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 380, -1, -1));

        jLabel17.setForeground(new java.awt.Color(0, 102, 204));
        jLabel17.setText("Ticket Title");
        add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        jLabel18.setForeground(new java.awt.Color(0, 102, 204));
        jLabel18.setText("Priority");
        add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, -1, -1));

        jLabel19.setForeground(new java.awt.Color(0, 102, 204));
        jLabel19.setText("Create Date");
        add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 320, -1, -1));

        ProjectNameFld.setForeground(new java.awt.Color(0, 102, 204));
        ProjectNameFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProjectNameFldActionPerformed(evt);
            }
        });
        add(ProjectNameFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, 180, -1));

        PCreateDateFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PCreateDateFldActionPerformed(evt);
            }
        });
        add(PCreateDateFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 100, 180, -1));

        ManagerFld.setForeground(new java.awt.Color(0, 102, 204));
        ManagerFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManagerFldActionPerformed(evt);
            }
        });
        add(ManagerFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 180, -1));

        PosterFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PosterFldActionPerformed(evt);
            }
        });
        add(PosterFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 180, -1));

        PStatusFld.setForeground(new java.awt.Color(0, 102, 204));
        PStatusFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PStatusFldActionPerformed(evt);
            }
        });
        add(PStatusFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 180, -1));

        jScrollPane2.setForeground(new java.awt.Color(0, 102, 204));

        PDescriptionTArea.setColumns(20);
        PDescriptionTArea.setForeground(new java.awt.Color(0, 102, 204));
        PDescriptionTArea.setRows(5);
        jScrollPane2.setViewportView(PDescriptionTArea);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 190, 467, -1));

        TicketTitleFld.setForeground(new java.awt.Color(0, 102, 204));
        TicketTitleFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TicketTitleFldActionPerformed(evt);
            }
        });
        add(TicketTitleFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 320, 180, -1));

        TCreateDateFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TCreateDateFldActionPerformed(evt);
            }
        });
        add(TCreateDateFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 320, 180, -1));

        PriorityFld.setForeground(new java.awt.Color(0, 102, 204));
        PriorityFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PriorityFldActionPerformed(evt);
            }
        });
        add(PriorityFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 350, 180, -1));

        TypeFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TypeFldActionPerformed(evt);
            }
        });
        add(TypeFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 350, 180, -1));

        ReporterFld.setForeground(new java.awt.Color(0, 102, 204));
        ReporterFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReporterFldActionPerformed(evt);
            }
        });
        add(ReporterFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 380, 180, -1));

        AssignerFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AssignerFldActionPerformed(evt);
            }
        });
        add(AssignerFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 380, 180, -1));

        TStatusFld.setForeground(new java.awt.Color(0, 102, 204));
        TStatusFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TStatusFldActionPerformed(evt);
            }
        });
        add(TStatusFld, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 410, 180, -1));

        jScrollPane3.setForeground(new java.awt.Color(0, 102, 204));

        TDescriptionTArea.setColumns(20);
        TDescriptionTArea.setForeground(new java.awt.Color(0, 102, 204));
        TDescriptionTArea.setRows(5);
        jScrollPane3.setViewportView(TDescriptionTArea);

        add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 450, 467, -1));

        jLabel2.setForeground(new java.awt.Color(0, 102, 204));
        jLabel2.setText("---------------------------------------------------------------------------------");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jLabel5.setForeground(new java.awt.Color(0, 102, 204));
        jLabel5.setText("--------------------------TERIYAKI Version 2.0 -------------------------");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 660, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void BackBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackBtnActionPerformed
        // TODO add your handling code here:
        DPC_VMT viewMyTicket = new DPC_VMT(mainFrame);
        mainFrame.setContentPane(viewMyTicket);   
        viewMyTicket.updateUI();
    }//GEN-LAST:event_BackBtnActionPerformed

    private void ProjectNameFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProjectNameFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ProjectNameFldActionPerformed

    private void PCreateDateFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PCreateDateFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PCreateDateFldActionPerformed

    private void ManagerFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManagerFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ManagerFldActionPerformed

    private void PosterFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PosterFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PosterFldActionPerformed

    private void PStatusFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PStatusFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PStatusFldActionPerformed

    private void TicketTitleFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TicketTitleFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TicketTitleFldActionPerformed

    private void TCreateDateFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TCreateDateFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TCreateDateFldActionPerformed

    private void PriorityFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PriorityFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PriorityFldActionPerformed

    private void TypeFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TypeFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TypeFldActionPerformed

    private void ReporterFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReporterFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ReporterFldActionPerformed

    private void AssignerFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AssignerFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AssignerFldActionPerformed

    private void TStatusFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TStatusFldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TStatusFldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AssignerFld;
    private javax.swing.JButton BackBtn;
    private javax.swing.JTextField ManagerFld;
    private javax.swing.JTextField PCreateDateFld;
    private javax.swing.JTextArea PDescriptionTArea;
    private javax.swing.JTextField PStatusFld;
    private javax.swing.JTextField PosterFld;
    private javax.swing.JTextField PriorityFld;
    private javax.swing.JTextField ProjectNameFld;
    private javax.swing.JTextField ReporterFld;
    private javax.swing.JTextField TCreateDateFld;
    private javax.swing.JTextArea TDescriptionTArea;
    private javax.swing.JTextField TStatusFld;
    private javax.swing.JTextField TicketTitleFld;
    private javax.swing.JTextField TypeFld;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    // End of variables declaration//GEN-END:variables
}
