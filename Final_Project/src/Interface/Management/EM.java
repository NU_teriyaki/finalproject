/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Management;

import Interface.main.MainFrame;
import Model.Interface.Employee;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author guhandan
 */
public class EM extends javax.swing.JPanel {

    /**
     * Creates new form EM
     */
    private MainFrame mainFrame;
    public EM(MainFrame mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        refreshTable();
    }
    
    public void refreshTable() {
        int rowCount = this.jTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (Employee e : mainFrame.getLogStatus().getCompany().getEmployees()) {
            Object row[] = new Object[4];
            row[0] = e.getUID();
            row[1] = e;
            row[2] = e.getOccupation();
            row[3] = e.getDepartment();
            model.addRow(row);
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        HireBtn = new javax.swing.JButton();
        ViewBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/src/navigation.png"))); // NOI18N
        jLabel1.setText("Management >> Employee Management");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jScrollPane1.setForeground(new java.awt.Color(0, 102, 204));

        jTable.setForeground(new java.awt.Color(0, 102, 204));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Employee ID", "Name", "Occupation", "Department"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 545, 110));

        HireBtn.setBackground(new java.awt.Color(255, 255, 255));
        HireBtn.setForeground(new java.awt.Color(0, 102, 204));
        HireBtn.setText("HIRE");
        HireBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HireBtnActionPerformed(evt);
            }
        });
        add(HireBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(294, 232, -1, -1));

        ViewBtn.setBackground(new java.awt.Color(255, 255, 255));
        ViewBtn.setForeground(new java.awt.Color(0, 102, 204));
        ViewBtn.setText("VIEW");
        ViewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewBtnActionPerformed(evt);
            }
        });
        add(ViewBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(431, 232, -1, -1));

        jLabel2.setForeground(new java.awt.Color(0, 102, 204));
        jLabel2.setText("---------------------------------------------------------------------------------");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jLabel5.setForeground(new java.awt.Color(0, 102, 204));
        jLabel5.setText("--------------------------TERIYAKI Version 2.0 -------------------------");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 660, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void HireBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HireBtnActionPerformed
        // TODO add your handling code here:
        EM_Hire c = new EM_Hire(mainFrame);
        mainFrame.setContentPane(c);
        c.updateUI();
    }//GEN-LAST:event_HireBtnActionPerformed

    private void ViewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewBtnActionPerformed
        // TODO add your handling code here:
        if(jTable.getSelectedRow() < 0){
            JOptionPane.showMessageDialog(null, "Please Select!", "WARNING", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Employee e = (Employee) jTable.getValueAt(jTable.getSelectedRow(), 1);
        EM_View d = new EM_View(mainFrame,e);
        mainFrame.setContentPane(d);
        d.updateUI();
    }//GEN-LAST:event_ViewBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton HireBtn;
    private javax.swing.JButton ViewBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
