/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Interface.Management.Charts;

import Model.EcoSystem.Transaction;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Lins
 */
public class PieChart {
    ChartPanel frame1;

    public PieChart(String title, List<Transaction> rawData) {
        DefaultPieDataset data = getDataSet(rawData);
        JFreeChart chart = ChartFactory.createPieChart(title, data, true, false, false);
        //设置百分比
        PiePlot pieplot = (PiePlot) chart.getPlot();
        DecimalFormat df = new DecimalFormat("0.00%");//获得一个DecimalFormat对象，主要是设置小数问题
        NumberFormat nf = NumberFormat.getNumberInstance();//获得一个NumberFormat对象
        StandardPieSectionLabelGenerator sp1 = new StandardPieSectionLabelGenerator("{0}  {2}", nf, df);//获得StandardPieSectionLabelGenerator对象
        pieplot.setLabelGenerator(sp1);//设置饼图显示百分比

        //没有数据的时候显示的内容
        pieplot.setNoDataMessage("No Data Available");
        pieplot.setCircular(false);
        pieplot.setLabelGap(0.02D);

        pieplot.setIgnoreNullValues(true);//设置不显示空值
        pieplot.setIgnoreZeroValues(true);//设置不显示负值
        frame1 = new ChartPanel(chart, true);
    }
    
    private static DefaultPieDataset getDataSet(List<Transaction> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        Map<String, Double> val = new HashMap<String, Double>();
        for(Transaction t : data){
            if(val.containsKey(t.getType())){
                val.put(t.getType(), val.get(t.getType())+t.getAmount());
           }else{
                val.put(t.getType(), t.getAmount());
            }
        }
        for(String type : val.keySet()){
            dataset.setValue(type, val.get(type));
        }
        return dataset;
    }

    public ChartPanel getChartPanel() {
        return frame1;

    }
}