/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Interface.Management.Charts;

import Model.EcoSystem.Financial;
import Model.EcoSystem.Transaction;
import java.awt.Font;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
/**
 *
 * @author Lins
 */
public class BarChart {
    ChartPanel frame1;

    public BarChart(String title, Financial financial) {
        CategoryDataset dataset = getDataSet(financial);
        JFreeChart chart = ChartFactory.createBarChart(
                title, // 图表标题
                "Transaction Type", // 目录轴的显示标签
                "Amount", // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                false, // 是否显示图例(对于简单的柱状图必须是false)
                false, // 是否生成工具
                false // 是否生成URL链接
        );

        frame1 = new ChartPanel(chart, true);        //这里也可以用chartFrame,可以直接生成一个独立的Frame

    }

    private static CategoryDataset getDataSet(Financial financial) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        double incount=0;
        for(Transaction t:financial.getInTransactions()){
            incount += t.getAmount();
        }
        dataset.addValue(incount, "Income", "Income");
        
        double outcount=0;
        for(Transaction t:financial.getOutTransactions()){
            outcount += t.getAmount();
        }
        dataset.addValue(outcount, "Outcome", "Outcome");
        dataset.addValue(incount-outcount, "Profit", "Profit");
        return dataset;
    }

    public ChartPanel getChartPanel() {
        return frame1;

    }
}
