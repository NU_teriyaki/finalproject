/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Interface;

import java.util.List;

/**
 *
 * @author Lins
 */
public interface Department {
    public Employee getEmployee();
    public List<Employee> getAllEmployee();
    public void newEmployee(Employee employee);
    public String getName();
}
