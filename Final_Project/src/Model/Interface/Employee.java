/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Interface;

import Model.TicketSystem.Ticket;
import java.util.List;
import javax.swing.JMenuBar;

/**
 *
 * @author Lins
 */
public interface Employee {

    public String getUID();

    public String getName();

    public void setName(String name);

    public String getPassword();

    public void setPassword(String password);

    public void setOccupation(String occupation);

    public String getOccupation();

    public void setDepartment(String department);

    public String getDepartment();

    public List<Ticket> getTickets();
    // public List<Mail> getMails();
    
    public void copy_value(Employee e);
    
    public void setUID(String UID);
    
    public String getPicPath();
    
    public void setPicPath(String picPath);
    
    public void jobFinished();
    
    public int getFinishedJobs();
}
