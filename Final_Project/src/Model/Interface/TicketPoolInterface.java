/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Interface;

import Model.TicketSystem.Ticket;
import Model.Users.User;
import java.util.List;

/**
 *
 * @author Lins
 */
public interface TicketPoolInterface extends PoolInterface{
    public List<Ticket> getMyTickets(User user);
    public List<Ticket> getSentToMeTickets(User user);
}
