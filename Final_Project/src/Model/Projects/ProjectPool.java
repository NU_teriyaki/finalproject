/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Projects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Lins
 */
public class ProjectPool {
    private Map<String, Project> projMap;
    
    public ProjectPool(){
        projMap = new HashMap<>();
    }
    
    public List<String> getProjectList(){
        return new ArrayList<>(projMap.keySet());
    }
    
    public Project getProject(String name){
        return projMap.get(name);
    }
    
    public boolean addProject(String name, Project proj){
        if(projMap.containsKey(name)){
            return false;
        }else{
            projMap.put(name, proj);
            return true;
        }
    }
    
}
