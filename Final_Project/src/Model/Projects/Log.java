/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Projects;

import java.util.Date;

/**
 *
 * @author Lins
 */
public class Log {
    private Date createDate;
    private String version;
    private String type;
    private String commit;

    public Log(String version, String type, String commit) {
        this.version = version;
        this.type = type;
        this.commit = commit;
        this.createDate = new Date();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCommit() {
        return commit;
    }

    public void setCommit(String commit) {
        this.commit = commit;
    }
    
    @Override
    public String toString() {
        return version;
    }
}
