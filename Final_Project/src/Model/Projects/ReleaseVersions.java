package Model.Projects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */

/**
 *
 * @author Lins
 */
public class ReleaseVersions {
    private String currentStatus;
    private List<Log> releaseLog;

    public ReleaseVersions() {
        currentStatus = "NEW";
        releaseLog = new ArrayList<>();
    }
    
    public String getStatus(){
        return this.currentStatus;
    }
    
    public void releaseNewVersion(String ver, String commit, String type){
        currentStatus = ver;
        releaseLog.add(new Log(ver, type, commit));
    }

    public List<Log> getReleaseLog() {
        return releaseLog;
    }
}
