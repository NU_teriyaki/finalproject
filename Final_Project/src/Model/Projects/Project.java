/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Projects;

import Model.TicketSystem.WorkTicket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lins
 */
public class Project {
    private String name;
    private double budget;
    private String client;
    private String leaderUID;
    private String description;
    private Date createDate;
    private List<String> memberUIDList;
    private List<WorkTicket> tickets;
    private ReleaseVersions log;

    public Project() {
        this.memberUIDList = new ArrayList<>();
        this.tickets = new ArrayList<>();
        this.createDate = new Date();
        this.log = new ReleaseVersions();
    }
    
    public List<Log> getLog(){
        return this.log.getReleaseLog();
    }
    
    public void releaseNewVersion(String ver,String commit,String type){
        this.log.releaseNewVersion(ver, commit, type);
    }
    
    public String getStatus(){
        return log.getStatus();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public List<WorkTicket> getTickets() {
        return tickets;
    }
    
    public void addTicket(WorkTicket wt){
        this.tickets.add(wt);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getLeaderUID() {
        return leaderUID;
    }

    public void setLeaderUID(String leaderUID) {
        this.leaderUID = leaderUID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void addMember(String UID){
        this.memberUIDList.add(UID);
    }

    public List<String> getMemberUIDList() {
        return memberUIDList;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
    
}
