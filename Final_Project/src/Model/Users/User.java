/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Users;

import Model.TicketSystem.Ticket;
import Model.TicketSystem.TicketPool;
import Utils.genID;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import Model.Interface.Employee;
import javax.swing.JMenuBar;

/**
 *
 * @author Lins
 */
public class User implements Employee{
    protected String name;
    protected String UID;
    protected String password;
    protected String department;
    protected String occupation;
    protected List<Ticket> tickets;
    protected String picPath;
    protected int finshedJobs;

    public User() {
        this.tickets = new ArrayList<>();
        this.finshedJobs = 0;
    }
    
    @Override
    public void jobFinished(){
        this.finshedJobs += 1;
    }
    
    @Override
    public int getFinishedJobs(){
        return finshedJobs;
    }
    
    @Override
    public String getUID() {
        return this.UID;
    }
    
    @Override
    public void setUID(String UID){
        this.UID = UID;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String getOccupation() {
        return this.occupation;
    }

    @Override
    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String getDepartment() {
        return this.department;
    }

    @Override
    public List<Ticket> getTickets() {
        //this.tickets = getNewTickets();
        return this.tickets;
    }
    
    @Override
    public void copy_value(Employee e){
        this.name = e.getName();
        this.password = e.getPassword();
        this.department = e.getDepartment();
        this.occupation = e.getOccupation();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getPicPath() {
        return this.picPath;
    }
    
    @Override
    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }
}
