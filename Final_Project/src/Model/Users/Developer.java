/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.Users;

import Model.TicketSystem.Ticket;
import java.util.List;
import Model.Interface.Employee;
import java.util.ArrayList;

/**
 *
 * @author Lins
 */
public class Developer extends User{

    private List<Ticket> solvedTickets;
    private List<String> involvedProjects;
    
    public Developer() {
        super();
        solvedTickets = new ArrayList<>();
        involvedProjects = new ArrayList<>();
    }
    
}
