/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lins
 */
public class Financial {
    private List<Transaction> inTransactions;
    private List<Transaction> outTransactions;
    private List<Transaction> allTransactions;

    public Financial() {
        inTransactions = new ArrayList<>();
        outTransactions = new ArrayList<>();
        allTransactions = new ArrayList<>();
    }
    
    public void newIncome(Transaction transaction){
        inTransactions.add(transaction);
        allTransactions.add(transaction);
    }
    
    public void newOutcome(Transaction transaction){
        outTransactions.add(transaction);
        allTransactions.add(transaction);
    }

    public List<Transaction> getInTransactions() {
        return inTransactions;
    }

    public List<Transaction> getOutTransactions() {
        return outTransactions;
    }
    
    public List<Transaction> getAllTransactions(){
        return allTransactions;
    }
}
