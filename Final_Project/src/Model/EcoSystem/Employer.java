/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import Model.Interface.Employee;
import Model.Users.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lins
 */
public class Employer extends User{
    private List<Company> companies;
    
    public Employer(){
        super();
        // Single Company
        this.companies = new ArrayList<>();
        this.department = "Employer";
        this.occupation = "Employer";
    }

    public void newCompany(Company c){
        this.companies.add(c);
    }

    public List<Company> getCompanies() {
        return companies;
    }
    
}
