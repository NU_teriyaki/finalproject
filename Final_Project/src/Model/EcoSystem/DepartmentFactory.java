/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import Model.EcoSystem.Deparments.AccountantDepartment;
import Model.EcoSystem.Deparments.AdminDepartment;
import Model.EcoSystem.Deparments.DeveloperDepartment;
import Model.EcoSystem.Deparments.PMDepartment;
import Model.EcoSystem.Deparments.RecruiterDepartment;
import Model.Interface.Department;

/**
 *
 * @author Lins
 */
public class DepartmentFactory {
    private static DepartmentFactory factory;
    
    private DepartmentFactory(){
        
    }
    
    public static DepartmentFactory getInstance(){
        if(factory == null)
            factory = new DepartmentFactory();
        return factory;
    }
    
    public Department createNewDepartment(String title){
        if (title == null) {
            return null;
        }
        if (title.equalsIgnoreCase("DEVDEPARTMENT")) {
            return new DeveloperDepartment();
        } else if (title.equalsIgnoreCase("PMDEPARTMENT")) {
            return new PMDepartment();
        } else if (title.equalsIgnoreCase("RECRUITERDEPARTMENT")) {
            return new RecruiterDepartment();
        } else if (title.equalsIgnoreCase("ADMINDEPARTMENT")) {
            return new AdminDepartment();
        } else if (title.equalsIgnoreCase("ACCOUNTANTDEPARTMENT")) {
            return new AccountantDepartment();
        }
        return null;
    }
}
