/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import Model.Interface.Department;
import Model.Interface.Employee;
import Model.Projects.Project;
import Model.Projects.ProjectPool;
import Model.TicketSystem.OATicket;
import Model.TicketSystem.Ticket;
import Model.TicketSystem.WorkTicket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;


/**
 *
 * @author Lins
 */
public class Company {
    
    private String name;
    private List<Employee> employees;
    private List<Property> properties;
    private ProjectPool projects;
    private List<String> postedJobs;
    private Financial financial;
    private EmployeeFactory HumanResource;
    
    // TODO can be more abstract
    
    private DepartmentFactory CompanyManagement;
    
    private Department adminDepartment;
    private Department devDepartment;
    private Department accountantDepartment;
    private Department pmDepartment;
    private Department recruiterDepartment;
    
    public List<OATicket> tickets;
    
    public Company(){
        employees = new ArrayList<>();
        properties = new ArrayList<>();
        projects = new ProjectPool();
        postedJobs = new ArrayList<>();
        financial = new Financial();
        HumanResource = EmployeeFactory.getInstance();
        CompanyManagement = DepartmentFactory.getInstance();
        tickets = new ArrayList<>();
        initDepartment();
//        initProperty();
    }
    
    private void initDepartment(){
        this.adminDepartment = CompanyManagement.createNewDepartment("ADMINDEPARTMENT");
        this.devDepartment = CompanyManagement.createNewDepartment("DEVDEPARTMENT");
        this.accountantDepartment = CompanyManagement.createNewDepartment("ACCOUNTANTDEPARTMENT");
        this.recruiterDepartment = CompanyManagement.createNewDepartment("RECRUITERDEPARTMENT");
        this.pmDepartment = CompanyManagement.createNewDepartment("PMDEPARTMENT");
    }

    public Department getAdminDepartment() {
        return adminDepartment;
    }

    public Department getDevDepartment() {
        return devDepartment;
    }

    public Department getAccountantDepartment() {
        return accountantDepartment;
    }

    public Department getPmDepartment() {
        return pmDepartment;
    }

    public Department getRecruiterDepartment() {
        return recruiterDepartment;
    }
    
    
    
    public void newOATicket(OATicket ticket){
        this.tickets.add(ticket);
    }

    public List<OATicket> getOATickets() {
        return tickets;
    }
    
    private void initProperty(){
        Property p1 = new Property("A");
        Property p2 = new Property("B");
        Property p3 = new Property("C");
        properties.add(p1);
        properties.add(p2);
        properties.add(p3);
    }
    
    public boolean duplicationCheck(String name){
        boolean flag = true;
        for(Employee e:employees){
            if(e.getName().equals(name)){
                flag = false;
            }
        }
        return flag;
    }
    
    public Employee hireNew(String type){
        Employee e = HumanResource.hireNewEmployee(type);
        e.setDepartment(type);
        employees.add(e);
        //Admin, Developer, PM, Recruiter, Accoutant
        if(e.getDepartment().equals("Admin"))
            adminDepartment.newEmployee(e);
        if(e.getDepartment().equals("Developer"))
            devDepartment.newEmployee(e);
        if(e.getDepartment().equals("PM"))
            pmDepartment.newEmployee(e);
        if(e.getDepartment().equals("Recruiter"))
            recruiterDepartment.newEmployee(e);
        if(e.getDepartment().equals("Accountant"))
            accountantDepartment.newEmployee(e);
        
        return e;
    }
    
    public Employee getEmployee(String type){
        if(type.equals("Admin"))
            return this.adminDepartment.getEmployee();
        if(type.equals("Developer"))
            return devDepartment.getEmployee();
        if(type.equals("PM"))
            return pmDepartment.getEmployee();
        if(type.equals("Recruiter"))
            return recruiterDepartment.getEmployee();
        if(type.equals("Accountant"))
            return accountantDepartment.getEmployee();
        return null;
    }

    public String getName() {
        return name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public List<Property> getProperties() {
        return properties;
    }
    
    public boolean newProject(Project project){
        return this.projects.addProject(project.getName(), project);
    }
    
    public List<String> getProjects() {
        return projects.getProjectList();
    }
    
    public ProjectPool getProjectPool() {
        return projects;
    }

    public List<String> getPostedJobs() {
        return postedJobs;
    }

    public Financial getFinancial() {
        return financial;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Employee findEmployee(String name){
        for(Employee e:this.employees){
            if(e.getName().equals(name)){
                return e;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
