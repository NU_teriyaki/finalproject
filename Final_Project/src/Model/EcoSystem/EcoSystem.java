/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import DAO.DB4OUtil;
import Model.MailSystem.Mail;
import Model.MailSystem.MailPool;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Lins
 */
public class EcoSystem {
    
    private static EcoSystem persons;
    private Map<String, Employer> employers;
    private List<Job> jobs;
    private MailPool mailPool;
    private Set<String> companyCatalog;
    private EcoSystem(){
        // TODO: DB4O dump&retreive
        employers = new HashMap<>();
        jobs = new ArrayList<>();
        mailPool = new MailPool();
        companyCatalog = new HashSet<>();
    }
    
    public static EcoSystem getInstance(){
        if(persons == null)
            persons = new EcoSystem();
        return persons;
    }
    
    public boolean duplicationCheck(String name){
        boolean flag = true;
        for(String n:employers.keySet()){
            if(n.equals(name)){
                flag = false;
            }
        }
        return flag;
    }
    
    public void newEmployer(Employer e){
        // TODO: duplication check
        this.employers.put(e.getName(), e);
    }

    public Map<String, Employer> getEmployers() {
        return employers;
    }
    
    // TODO: modify,lookup,delete
    
    public void newJob(Job j){
        this.jobs.add(j);
    }

    public List<Job> getJobs() {
        return jobs;
    }
    
    public boolean companyNameCheck(String name){
        if(companyCatalog.contains(name)){
            return false;
        }else{
            companyCatalog.add(name);
            return true;
        }
    }
    
    // TODO:
    public void sendMail(String sender, String receiver, String title, String content){
        Mail mail = new Mail();
        mail.setSenderUID(sender);
        mail.setReceiverUID(receiver);
        mail.setTitle(title);
        mail.setContent(content);
        this.mailPool.newMail(receiver, mail);
    }
    
    public List<Mail> receiveMails(String receiver){
        return this.mailPool.getMails(receiver);
    }
}
