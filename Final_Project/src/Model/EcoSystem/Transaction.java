/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import Utils.genID;
import java.util.Date;

/**
 *
 * @author Lins
 */

public class Transaction {
    private String transactionID;
    private double amount;
    private String type;
    private String relatedUID;
    private String chargedProj;
    private String relatedTID;
    private Date date;
    
    
    public Transaction(double amount, String type, String relatedUID, String chargedProj, String relatedTID) {
        this.transactionID = genID.getInstance().gen();
        this.amount = amount;
        this.type = type;
        this.relatedUID = relatedUID;
        this.chargedProj = chargedProj;
        this.relatedTID = relatedTID;
        this.date = new Date();
    }

    public Date getDate() {
        return date;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getRelatedUID() {
        return relatedUID;
    }

    public String getChargedProj() {
        return chargedProj;
    }

    public String getRelatedTID() {
        return relatedTID;
    }

    @Override
    public String toString() {
        return transactionID;
    }
    
    
    
}
