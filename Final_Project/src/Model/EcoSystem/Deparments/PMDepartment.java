/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem.Deparments;

import Model.Interface.Department;
import Model.Interface.Employee;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lins
 */
public class PMDepartment implements Department {

    private List<Employee> employees;

    public PMDepartment() {
        employees = new ArrayList<>();
    }

    @Override
    public Employee getEmployee() {
        if (this.employees.size() == 0) {
            return null;
        }
        int index = (int) (Math.random() * this.employees.size());
        return this.employees.get(index);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employees;
    }

    @Override
    public void newEmployee(Employee employee) {
        employee.setDepartment("PM");
        this.employees.add(employee);
    }

    @Override
    public String getName() {
        return "PM";
    }

}
