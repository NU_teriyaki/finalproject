/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import java.util.Date;

/**
 *
 * @author Lins
 */
public class Job {
    private String name;
    private String poster;
    private String description;
    private double budget;
    private Date postDate;
    
    public Job(){
        this.postDate = new Date();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public Date getPostDate() {
        return postDate;
    }

    @Override
    public String toString() {
        return name;
    }
    
}
