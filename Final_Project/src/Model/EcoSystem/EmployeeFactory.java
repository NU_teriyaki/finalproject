/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.EcoSystem;

import Model.Users.Accountant;
import Model.Users.Admin;
import Model.Users.Developer;
import Model.Users.ProjectManager;
import Model.Users.Recruiter;
import Model.Interface.Employee;

/**
 *
 * @author Lins
 */
public class EmployeeFactory {
    
    private static EmployeeFactory factory;
    
    private EmployeeFactory(){
    }
    
    public static EmployeeFactory getInstance(){
        if(factory == null)
            factory = new EmployeeFactory();
        return factory;
    }

    public Employee hireNewEmployee(String department) {
        if (department == null) {
            return null;
        }
        if (department.equalsIgnoreCase("DEVELOPER")) {
            return new Developer();
        } else if (department.equalsIgnoreCase("PM")) {
            return new ProjectManager();
        } else if (department.equalsIgnoreCase("RECRUITER")) {
            return new Recruiter();
        } else if (department.equalsIgnoreCase("ADMIN")) {
            return new Admin();
        } else if (department.equalsIgnoreCase("ACCOUNTANT")) {
            return new Accountant();
        }
        return null;
    }
}
