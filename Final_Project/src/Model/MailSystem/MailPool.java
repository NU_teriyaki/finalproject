/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.MailSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import javax.management.monitor.StringMonitor;

/**
 *
 * @author Lins
 */
public class MailPool {
    private Map<String, List<Mail>> mailpool;

    public MailPool() {
        mailpool = new HashMap<>();
    }
    
    public void newMail(String receiver, Mail mail){
        if (mailpool.containsKey(receiver)) {
            mailpool.get(receiver).add(mail);
        }else{
            List<Mail> mailList = new ArrayList<>();
            mailList.add(mail);
            mailpool.put(receiver, mailList);
        }
    }
    
    public List<Mail> getMails(String receiver){
        if(!mailpool.containsKey(receiver))
            return new ArrayList<>();
        return mailpool.get(receiver);
    }
}
