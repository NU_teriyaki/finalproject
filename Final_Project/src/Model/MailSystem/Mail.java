/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.MailSystem;

import java.util.Date;

/**
 *
 * @author Lins
 */
public class Mail {
    private String senderUID;
    private String receiverUID;
    private String content;
    private String title;
    private Date sendDate;
    private boolean read;
       
    public Mail() {
        this.sendDate = new Date();
        this.read = false;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public String getSenderUID() {
        return senderUID;
    }

    public void setSenderUID(String senderUID) {
        this.senderUID = senderUID;
    }

    public String getReceiverUID() {
        return receiverUID;
    }

    public void setReceiverUID(String receiverUID) {
        this.receiverUID = receiverUID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
    
    @Override
    public String toString() {
        return senderUID;
    }
    
    
    
    
    
}
