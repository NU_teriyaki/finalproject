/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.TicketSystem;

import Utils.genID;
import java.util.Date;

/**
 *
 * @author Lins
 */
public abstract class Ticket {
    private String TicketID;
    private Date createDate;
    private String reporterID;
    private String assignerID;
    private String content;
    private Type type;
    
    public enum Type{
        OA("OA"), WORK("WORK");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Ticket(String reporterID, String assignerID, String content) {
        this.TicketID = genID.getInstance().gen().substring(0, 8);
        this.createDate = new Date();
        this.reporterID = reporterID;
        this.assignerID = assignerID;
        this.content = content;
    }

    public String getTicketID() {
        return TicketID;
    }

    public void setTicketID(String TicketID) {
        this.TicketID = TicketID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getReporterID() {
        return reporterID;
    }

    public void setReporterID(String reporterID) {
        this.reporterID = reporterID;
    }

    public String getAssignerID() {
        return assignerID;
    }

    public void setAssignerID(String assignerID) {
        this.assignerID = assignerID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        return TicketID;
    }
    
}
