/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.TicketSystem;

/**
 *
 * @author Lins
 */
public class TicketStatus {
    public enum WorkStatus{
        OPEN("OPEN"), PENDING("PENDING"), WIP("WIP"), DONE("DONE"), CLOSED("CLOSED");
        private String value;
        private WorkStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    public enum ApplyStatus{
        PENDINGLEADER, PENDINGBOSS, PENDINGPROCESS, CLOSED, REJECTED
    }
    
}


