/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.TicketSystem;

import java.util.Date;

/**
 *
 * @author Lins
 */
public class WorkTicket extends Ticket{
    
    private String title;
    private String relatedProj;
    private int priority;
    private TicketStatus.WorkStatus workStatus;
    private String workType;
    
    public WorkTicket(String reporterID, 
            String assignerID, String content, String relatedProj, String title) {
        super(reporterID, assignerID, content);
        this.relatedProj = relatedProj;
        this.priority = 3;
        this.title = title;
        this.workStatus = TicketStatus.WorkStatus.OPEN;
    }

    public TicketStatus.WorkStatus getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(TicketStatus.WorkStatus workStatus) {
        this.workStatus = workStatus;
    }
    
    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelatedProj() {
        return relatedProj;
    }

    public void setRelatedProj(String relatedProj) {
        this.relatedProj = relatedProj;
    }
    
    public int getPriority() {
        return priority;
    }
    
    public void setPriority(int pri){
        this.priority = pri;
    }

    @Override
    public String toString() {
        return title;
    }
    
    
}
