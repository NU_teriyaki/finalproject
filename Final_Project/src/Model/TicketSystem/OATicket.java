/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.TicketSystem;

import Model.EcoSystem.Property;
import Model.EcoSystem.Transaction;

/**
 *
 * @author Lins
 */
public class OATicket extends Ticket{
    private TicketStatus.ApplyStatus status;
    private Property property;
    private Transaction transaction;
    private String OAType;
    private String projectName;
    private double amount;

    public OATicket(OATicket ticket){
        super(ticket.getReporterID(), ticket.getAssignerID(), ticket.getContent());
        this.setTicketID(ticket.getTicketID());
        this.setType(ticket.getType());
        this.status = ticket.getStatus();
        this.property = ticket.getProperty();
        this.transaction = ticket.getTransaction();
        this.OAType = ticket.getOAType();
    }
    
    public OATicket(String reporterID, String assignerID, String content) {
        super(reporterID, assignerID, content);
    }

    public String getOAType() {
        return OAType;
    }

    public void setOAType(String OAType) {
        this.OAType = OAType;
    }

    public TicketStatus.ApplyStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus.ApplyStatus status) {
        this.status = status;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
    
}
