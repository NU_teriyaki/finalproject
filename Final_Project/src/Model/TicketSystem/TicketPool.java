/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Model.TicketSystem;

import Model.Interface.TicketPoolInterface;
import Model.Users.User;
import java.util.List;

/**
 *
 * @author Lins
 */
public class TicketPool implements TicketPoolInterface{
    
    private static TicketPool pool;
    
    private TicketPool(){
        
    }
    
    public static TicketPool getInstance(){
        if(pool == null)
            pool = new TicketPool();
        return pool;
    }

    @Override
    public List<Ticket> getMyTickets(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Ticket> getSentToMeTickets(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
