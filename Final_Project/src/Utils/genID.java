/*
 * Author:Haoyu Lin
 * Contact:haoyulin96@gmail.com
 * Copyright.
 */
package Utils;

import java.util.UUID;

/**
 *
 * @author Lins
 */
public class genID {
    private static genID generator;
    
    public static genID getInstance(){
        if(generator == null)
            generator = new genID();
        return generator;
    }
    public String gen(){
        return UUID.randomUUID().toString().substring(0, 8);
    }
}
